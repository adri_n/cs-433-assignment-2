#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <cstring>
#include <vector>
#include <stdlib.h>
using namespace std;
#define MAX_LINE 80 /* The maximum length command */

void parseInput(string &input, char *args[], bool &check);
void historyProcess(vector<string>, int);
void addInput(vector<string>&, string);
string getHistory(int, vector<string>, int);

int main()
{
  char *args[MAX_LINE/2 + 1]; /* command line arguments */
  vector<string> inputs;
  int shouldrun = 1; /* flag to determine when to exit program */
  pid_t pid;
  string userInput;
  int numOfInputs = 0;
  bool check = true;
  while (shouldrun != 0)
    {
      printf("osh> ");
      fflush(stdout);
      getline(cin,userInput);
      if(userInput == "!!")
	{
	  string tempHistory = getHistory(-1, inputs,numOfInputs); // -1 means most recent
	  userInput = tempHistory;
	}
      if(userInput[0] == '!' && userInput[1] != NULL) // If user inputs ! and a number
	{
	  string histPos = "";
	  for(int t = 1;t <userInput.size();t++)
	    histPos = histPos + userInput[t]; // Grabs number from user input
	  int numb = atoi(histPos.c_str()); //converts string to an int
	  string tempHistory = getHistory(numb,inputs,numOfInputs); 
	  userInput = tempHistory; // Sets userInput to the command received from getHistory()
	}
      if(userInput == "history")
      	historyProcess(inputs, numOfInputs);
      else if(userInput == "exit")
        shouldrun = 0;
      else
	{
	  addInput(inputs, userInput);
	  numOfInputs++;
	  parseInput(userInput, args, check);
	  pid = fork();
	  if(pid == 0) // Child process
	    {
	      execvp(args[0], args);
	      return 0;
	    }
	  else if (pid < 0) 
	    {
	      cout<<"Fork failed"<<endl;
	    }
	     
	  else // Parent process
	    {
	      if(check)
		wait(NULL);
	    }
	}
  }
  return 0;
}

/* This function takes a string by reference, then it reads through it using 4 variables. It reads the string into it reaches a space or the end.
   At this point, it makes a substring of the string starting from the beginning of the word (counter) to the length of the word (lengthOfWord).
   It then places this substring into a string array, the char* array is set equal to it, lengthOfWord is set back to 0, and it loops again. */
void parseInput(string &userInput, char *args[], bool &check)
{
  int start = 0; // Start holds location of char* array
  int counter = 0; // Keeps track of location on the string
  int lengthOfWord = 0; // Length of each word on the string
  int end = userInput.size();
  string inpArg[MAX_LINE/2 + 1];
  for(int i = 0;i < end;i++)
    {
      if(userInput[i] == '&')
	check = false;
      if(userInput[i] == ' ' || i == (end - 1))
	{
	  if(i == end-1) // Ensures that the the complete word is grabbed if its the only one
	    lengthOfWord++;
	  string temp = userInput.substr(counter,lengthOfWord); // Creates substring of the string containing a word in the string
	  inpArg[start] = temp; // Places the substring into a string array
	  args[start] = (char*)inpArg[start].c_str(); 
	  start++;
	  counter = i + 1;
	  lengthOfWord = 0;
	}
      lengthOfWord++;
    }
  args[start] = NULL; // Places NULL at the end
}

/* Displays the last commands entered by the user, with the most recent at the top. The num parameter represents the number of inputs */
void historyProcess(vector<string> inputs, int num)
{
  if(inputs.size() <= 10) // Case where there is less than 10 inputs in history
    {
      for(int i = inputs.size() - 1;i >=0; i--)
	{
	  cout << num << " " << inputs[i] << " " << endl;
	  num--;
	}
    }
  else
    {
      for(int i = inputs.size() - 1;i >= inputs.size()-10; i--)
	{
	  cout << num << " " << inputs[i] << " " << endl;
	  num--;
	}
    }
}

/* Gets a command from the history vector and returns it.*/
string getHistory(int pos, vector<string> inputs, int amountOfInputs)
{
  if(inputs.size() - 1 <= 0)
    cout << "No commands in history" << endl;
  if(pos == -1) // Case where user inputted !!
    {
      string temp = inputs[inputs.size()-1];
      return temp;
    }
  if(pos <= amountOfInputs && pos > amountOfInputs - 10)
    {
      string temp = inputs[pos - 1];
      return temp;
    }
  else
    {
      cout << "Not a valid command" << endl;
      return "";
    }
}

/* Adds an element to a vector*/
void addInput(vector<string>& inputs, string userInput)
{
  inputs.push_back(userInput);
}
